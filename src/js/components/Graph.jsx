var React = require('react');
var EmeraldStore = require('../stores/main.js');


var Graph = React.createClass({
    
    getInitialState: function() {
        return {
            caseResult: EmeraldStore.getCaseResult()
        };
    },

    componentDidMount: function() {
        var that = this;
        EmeraldStore.addChangeListener(function() {
            var caseResult = EmeraldStore.getCaseResult();
            that.setState({
                caseResult: caseResult
            });
        });
        this.setState({
            caseResult: EmeraldStore.getCaseResult()
        })
    },

    componentWillReceiveProps: function(props) {
        var roadStops = props.roadStops;
        var roadStopCount = roadStops.length;

        this.setState({
            roadStopCount : roadStopCount
        })
    },

    _getNumbersTop() {
        var i = 0;
        return this.state.caseResult.roadStops.map(function(roadStop) {
            i += 1;
            return (
                <div className="topNumberCell">
                    <div className="topNumberCellText">
                        {i}
                    </div>
                </div>
            )
        });
    },

    _getNumbersLeft() {
        var i = 0;
        return this.state.caseResult.roadStops.map(function(roadStop) {
            i += 1;
            return (
                <div className="leftNumberCell">
                    <div className="leftNumberCellText">
                        {i}
                    </div>
                </div>
            );
        });
    },

    _getCell(a, b) {
        var roads = this.state.caseResult.roadStops[a - 1].roadList;
        var className;
        if (roads.indexOf(b) > -1) {
            className = "graph-cell-green";
        } else {
            className = "graph-cell-red";
        }
        return (
            <div className={className}>
                
            </div>
        )
    },

    _getRow(cells) {
        return (
            <div className="graph-row">
                {cells}
            </div>
        );
    },

    render: function() {
        
        var numbersTop = this._getNumbersTop();
        var numbersLeft = this._getNumbersLeft();

        var roadStopCount = this.state.caseResult.roadStops.length;

        var rows = [];
        for (var a = 1; a < roadStopCount + 1; a++) {
            var columns = [];
            for (var b = 1; b < roadStopCount + 1; b++) {
                columns.push(this._getCell(a, b));
            }
            rows.push(this._getRow(columns));
        }

        return (
            <div className="graph-container">
                <div className="top-number-container">
                    <div className="empty-number-div" />
                    {numbersTop}
                </div>
                <div className="whole-graph-container">
                    <div className="left-number-container">
                        {numbersLeft}
                    </div>

                    <div className="graph-roads-div">
                        {rows}
                    </div>

                </div>
            </div>
        )
    }
})

module.exports = Graph;