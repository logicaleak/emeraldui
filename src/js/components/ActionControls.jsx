var React = require('react');
var EmeraldStore = require('../stores/main.js');

var actions = require('../actions/main.js');

var ActionControls = React.createClass({
    
    getInitialState: function() {
        return {
            whitePortals: [],
            blackPortals: []
        };
    },
        
    _makeWhitePortal: function(fr, to) {
        var whiteFrom = fr;
        var whiteTo = to;
        return (
            <div>
                {whiteFrom}->{whiteTo}
            </div>
        );
    },
    
    _addWhitePortal: function() {
        var whiteFrom = this.refs.whiteFrom.value;
        var whiteTo = this.refs.whiteTo.value; 
        this.state.whitePortals.push([whiteFrom, whiteTo]);
        this.setState({
            whitePortals : this.state.whitePortals
        }); 
    },
    _makeBlackPortal: function(fr, to) {
        var blackFrom = fr;
        var blackTo = to;
        return (
            <div>
                {blackFrom}->{blackTo}
            </div>
        );
    },
    
    _addBlackPortal: function() {
        var blackFrom = this.refs.blackFrom.value;
        var blackTo = this.refs.blackTo.value; 
        this.state.blackPortals.push([blackFrom, blackTo]);
        this.setState({
            blackPortals : this.state.blackPortals
        }); 
    },
    
    _analyzeRequest: function() {
        var whitePortals = this.state.whitePortals.map(function(whitePortal) {
            return {
                from : whitePortal[0],
                to : whitePortal[1]
            }
        });
        
        var blackPortals = this.state.blackPortals.map(function(blackPortal) {
            return {
                from : blackPortal[0],
                to : blackPortal[1]
            }
        });
        
        var newCase = {
            stopNumber: this.refs.stopNumber.value,
            diceLowerBound: this.refs.lowerDiceBound.value,
            diceUpperBound: this.refs.upperDiceBound.value,
            whitePortals: whitePortals,
            blackPortals: blackPortals
        }
        
        actions.START_NEW_CASE(newCase);
    },
    
    render: function() {
        var that = this;
        var whitePortalsJsx = this.state.whitePortals.map(function(whitePortal) {
            return that._makeWhitePortal(whitePortal[0], whitePortal[1]);
        });
        
        var blackPortalsJsx = this.state.blackPortals.map(function(blackPortal) {
            return that._makeBlackPortal(blackPortal[0], blackPortal[1]);
        });
         
        return (
            <div className="action-controls-container">
                <div className="stop-numbers-container">
                    <div> Stop Numbers </div>
                    <input type="number" ref="stopNumber" className="stop-number-input"/>
                </div>
                
                <div className="dice-bound-container">
                    <div>Lower Dice Bound </div>
                    <input type="number" ref="lowerDiceBound" className="dice-bound-input"/>
                    
                    <div>Upper Dice Bound </div>
                    <input type="number" ref="upperDiceBound" className="dice-bound-input"/>
                </div>
                
                <div className="white-portal-container">
                    <div className="portal-control">
                        <div>Add White Portal</div>
                        <input type="number" ref="whiteFrom" className="portal-input"/>
                        <input type="number" ref="whiteTo" className="portal-input"/>
                        <input type="button" value="add" onClick={this._addWhitePortal} />
                    </div>
                    <div>
                        {whitePortalsJsx}
                    </div>
                </div>
                
                    
                <div className="black-portal-container">
                    <div className="portal-control">    
                        <div>Add Black Portal</div>
                        <input type="number" ref="blackFrom" className="portal-input"/>
                        <input type="number" ref="blackTo" className="portal-input"/>
                        <input type="button" value="add" onClick={this._addBlackPortal} />
                    </div>
                    <div>
                        {blackPortalsJsx}
                    </div>
                </div>
                
                <div>
                    <input type="button" onClick={this._analyzeRequest} value="do"/>
                </div>
            </div>
        )
    }
});

module.exports = ActionControls;