var EventEmitter = require('events').EventEmitter;
var AppDispatcher = require('../dispatcher/AppDispatcher');
var _ = require('underscore');

var actions = require('../constants/main');


var EmeraldStore = _.extend({}, EventEmitter.prototype, {
    
    caseResult: {
        roadStops : [
            
        ],
        shortestPath: {}
    },

    getCaseResult: function() {
        return this.caseResult;
    },
    
    setCaseResult: function(caseResult) {
        this.caseResult = caseResult;
    },

	// Emit Change event
	emitChange: function() {
		this.emit('change');
	},

	// Add change listener
	addChangeListener: function(callback) {
		this.on('change', callback);
	},

	// Remove change listener
	removeChangeListener: function(callback) {
		this.removeListener('change', callback);
	},
    
    addExecutionListener: function(callback) {
        this.on('execution', callback);
    },
    
    emitExecution: function() {
        this.emit('execution');  
    }
});

AppDispatcher.register(function(action) {
    
    switch (action.actionType) {
        case actions.NEW_CASE_RETRIEVED:
            EmeraldStore.setCaseResult(action.data);
            EmeraldStore.emitChange();
            break;
    }
});
	
module.exports = EmeraldStore;