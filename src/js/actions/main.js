var AppDispatcher = require('../dispatcher/AppDispatcher');
var actionConstants = require('../constants/main.js');
var Ajax = require('../util/Ajax.js');

var actions = {
    
    START_NEW_CASE: function(newCaseJson) {
        var url = "http://localhost:8090/cityroad";
        Ajax.post_request(url, newCaseJson, function(data) {
            console.log("arrived");
            console.log(data);
            AppDispatcher.dispatch({
                actionType : actionConstants.NEW_CASE_RETRIEVED,
                data : data
            });
        });
    }
}

module.exports = actions;




