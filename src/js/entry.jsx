var render = require('react-dom').render;
var reactRouter = require('react-router');
var Graph = require('./components/Graph.jsx');
var ActionControls = require('./components/ActionControls.jsx');

var Router = reactRouter.Router;
var Route = reactRouter.Route;
var React = require('react');

var actions = require('./actions/main.js');

var AppDispatcher = require('./dispatcher/AppDispatcher.js');
var actionConstants = require('./constants/main');



var App = React.createClass({
    
    
	render: function() {

        var roadStops = [
            {
                number : 1,
                roads : [
                    {
                        to : 2,
                    },
                    {
                        to : 3
                    }
                ]       
            },
            {
                number : 2,
                roads : [
                    {
                        to : 3,
                    },
                    {
                        to : 4
                    }
                ]       
            },
            {
                number : 3,
                roads : [
                    {
                        to : 4,
                    }
                ]       
            },
            {
                number : 4,
                roads : [
                    
                ]       
            }
        ]

        var connections = {
            "1" : [
                2, 3
            ],
            "2" : [
                3, 4
            ],
            "3" : [
                4
            ],
            "4" : [

            ]
        }
        
		return (
			<div className="container">
                <div className="inputControlsDiv">
                    <ActionControls />
                </div>
                <div className="graphView">
                    <Graph roadStops={roadStops} connections={connections}/>
                </div>
                <div className="shortestPathView">

                </div>
            </div>
		)
	}
});

render((
  <Router>
    <Route path="/" component={App}>
      
    </Route>
  </Router>
), document.getElementById('container'));